package flight.controller;

import flight.dto.AirportDelayDto;
import flight.dto.CarrierDto;
import flight.dto.DayOfWeekDto;
import flight.dto.RouteDto;
import flight.service.FlightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by yunhangchen on 7/7/17.
 */
@RestController
@RequestMapping("/flights")
@CrossOrigin
public class FlightController {

    @Autowired
    FlightService flightService;

    @RequestMapping("/frequentRoute/rank/{ranking}")
    @ResponseBody
    public RouteDto getMostFrequentRoute(@PathVariable Integer ranking) {

        if (ranking <= 0) {
            throw new IllegalArgumentException("Ranking cannot be lower or equal to 0");
        }

        return flightService.getFrequentRouteWithRank(ranking - 1);
    }

    @RequestMapping("/carrier/rank/{ranking}")
    @ResponseBody
    public CarrierDto getBusyCarrier(@PathVariable Integer ranking) {

        if (ranking <= 0) {
            throw new IllegalArgumentException("Ranking cannot be lower or equal to 0");
        }

        return flightService.getNthBusyCarrier(ranking - 1);
    }

    @RequestMapping("/delay/departure/{ranking}")
    @ResponseBody
    public AirportDelayDto getDepartureDelayAirport(@PathVariable Integer ranking) {

        if (ranking <= 0) {
            throw new IllegalArgumentException("Ranking cannot be lower or equal to 0");
        }

        return flightService.getNthDepartureDelayAirport(ranking - 1);
    }

    @RequestMapping("/delay/arrival/{ranking}")
    @ResponseBody
    public AirportDelayDto getArrivalDelayAirport(@PathVariable Integer ranking) {

        if (ranking <= 0) {
            throw new IllegalArgumentException("Ranking cannot be lower or equal to 0");
        }

        return flightService.getNthArrivalDelayAirport(ranking - 1);
    }

    @RequestMapping("/dayOfWeek/{ranking}")
    @ResponseBody
    public DayOfWeekDto getBusyDayOfWeek(@PathVariable Integer ranking) {

        if (ranking <= 0) {
            throw new IllegalArgumentException("Ranking cannot be lower or equal to 0");
        }

        return flightService.getNthBusyDayOfWeek(ranking - 1);
    }
}
