package flight.dao;

import flight.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by yunhangchen on 7/7/17.
 */
@Repository
public class FlightDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String COUNT = "COUNT";

    private static final String AVERAGE_DEPARTURE_DELAY = "AVG_DEP_DELAY";

    private static final String AVERAGE_ARRIVAL_DELAY = "AVG_ARR_DELAY";

    private static final String DAY_OF_WEEK = "DAY_OF_WEEK";

    private static final String FREQUENT_ROUTE = "select ORIGIN_AIRPORT_ID, DEST_AIRPORT_ID, COUNT(*) AS COUNT FROM flight_info GROUP BY ORIGIN_AIRPORT_ID, DEST_AIRPORT_ID ORDER BY COUNT(*) DESC;";

    private static final String BUSY_CARRIER = "select CARRIER, COUNT(*) AS COUNT FROM flight_info GROUP BY CARRIER ORDER BY COUNT(*) DESC;";

    private static final String AIRPORT_DEPARTURE_DELAY = "select ORIGIN_AIRPORT_ID, AVG(DEP_DELAY_NEW) AS AVG_DEP_DELAY from flight_info GROUP BY ORIGIN_AIRPORT_ID ORDER BY AVG(DEP_DELAY_NEW) DESC;";

    private static final String AIRPORT_ARRIVAL_DELAY = "select DEST_AIRPORT_ID, AVG(ARR_DELAY_NEW) AS AVG_ARR_DELAY from flight_info GROUP BY DEST_AIRPORT_ID ORDER BY AVG(ARR_DELAY_NEW) DESC;";

    private static final String DAYOFWEEK_TRAVELS = "select DAYOFWEEK(FL_DATE) AS DAY_OF_WEEK, COUNT(*) AS COUNT FROM flight_info GROUP BY DAYOFWEEK(FL_DATE) ORDER BY COUNT(*) DESC;";

    public RouteDataObject getRankFrequentRoute(int rank) {
        List<Map<String, Object>> list = jdbcTemplate.queryForList(FREQUENT_ROUTE);
        if (list == null || list.isEmpty()) {
            throw new IllegalArgumentException("Most Frequent Route is empty!");
        }

        if (rank >= list.size()) {
            throw new IllegalArgumentException("Rank is out of the boundary of total routes");
        }

        Map<String, Object> map = list.get(rank);


        RouteDataObject route = new RouteDataObject();
        route.setDestAirportId((String) map.get("DEST_AIRPORT_ID"));
        route.setOrigAirportId((String) map.get("ORIGIN_AIRPORT_ID"));
        route.setFlightCount(((Long) map.get("COUNT")).intValue());

        return route;
    }

    public CarrierDto getRankBusiestCarrier(int rank) {
        List<Map<String, Object>> list = jdbcTemplate.queryForList(BUSY_CARRIER);
        if (list == null || list.isEmpty()) {
            throw new IllegalArgumentException("Busiest carrier is empty!");
        }

        if (rank >= list.size()) {
            throw new IllegalArgumentException("Rank is out of the boundary of number of carriers");
        }

        Map<String, Object> map = list.get(rank);

        CarrierDto carrierDto = new CarrierDto();
        carrierDto.setCode((String) map.get("CARRIER"));
        carrierDto.setTotalFlights(((Long) map.get(COUNT)).intValue());
        return carrierDto;

    }

    public AirportDelayDto getRandkDepartureAverageDelay(int rank) {
        List<Map<String, Object>> list = jdbcTemplate.queryForList(AIRPORT_DEPARTURE_DELAY);
        if (list == null || list.isEmpty()) {
            throw new IllegalArgumentException("No Departure Delay!");
        }

        if (rank >= list.size()) {
            throw new IllegalArgumentException("Rank is out of the boundary of number of airports with departure delays");
        }

        Map<String, Object> map = list.get(rank);


        String airportId = (String) map.get("ORIGIN_AIRPORT_ID");

        Map<String, Object> orig = jdbcTemplate.queryForList(RouteDto.SEARCH_ORIG_AIRPORT
                , new Object[] {airportId}).get(0);
        AirportDelayDto airportDelayDto = new AirportDelayDto();
        airportDelayDto.setAirport((String) orig.get("ORIGIN"));
        airportDelayDto.setAverageDelayMinutes(((Double) map.get(AVERAGE_DEPARTURE_DELAY)).intValue());
        return airportDelayDto;
    }

    public AirportDelayDto getRandkArrivalAverageDelay(int rank) {
        List<Map<String, Object>> list = jdbcTemplate.queryForList(AIRPORT_ARRIVAL_DELAY);
        if (list == null || list.isEmpty()) {
            throw new IllegalArgumentException("No Arrival Delay!");
        }

        if (rank >= list.size()) {
            throw new IllegalArgumentException("Rank is out of the boundary of number of airports with arrival delays");
        }
        Map<String, Object> map = list.get(rank);

        String airportId = (String) map.get("DEST_AIRPORT_ID");

        Map<String, Object> orig = jdbcTemplate.queryForList(RouteDto.SEARCH_DEST_AIRPORT
                , new Object[] {airportId}).get(0);
        AirportDelayDto airportDelayDto = new AirportDelayDto();
        airportDelayDto.setAirport((String) orig.get("DEST"));
        airportDelayDto.setAverageDelayMinutes(((Double) map.get(AVERAGE_ARRIVAL_DELAY)).intValue());
        return airportDelayDto;
    }

    public DayOfWeekDto getRankBusyDayOfWeek(int rank) {
        if (rank < 0 || rank > 6) {
            throw new IllegalArgumentException("Day of the week can only be 1 ~ 7");
        }

        List<Map<String, Object>> list = jdbcTemplate.queryForList(DAYOFWEEK_TRAVELS);
        if (list == null || list.isEmpty()) {
            throw new IllegalArgumentException("No Day of week !");
        }

        if (rank >= list.size()) {
            throw new IllegalArgumentException("Rank is out of the boundary of number of days of week");
        }

        Map<String, Object> map = list.get(rank);
        return new DayOfWeekDto((Integer) map.get(DAY_OF_WEEK), ((Long) map.get(COUNT)).intValue());
    }



}
