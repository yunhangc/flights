package flight.service;

import flight.dao.FlightDao;
import flight.dto.AirportDelayDto;
import flight.dto.CarrierDto;
import flight.dto.DayOfWeekDto;
import flight.dto.RouteDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

/**
 * Created by yunhangchen on 7/7/17.
 */
@Service
public class FlightService {

    @Autowired
    private FlightDao flightDao;

    @Autowired
    JdbcTemplate jdbcTemplate;

    public RouteDto getFrequentRouteWithRank(int rank) {
        return RouteDto.buildRouteDto(flightDao.getRankFrequentRoute(rank), jdbcTemplate);
    }

    public CarrierDto getNthBusyCarrier(int rank) {
        return flightDao.getRankBusiestCarrier(rank);
    }

    public AirportDelayDto getNthDepartureDelayAirport(int rank) {
        return flightDao.getRandkDepartureAverageDelay(rank);
    }

    public AirportDelayDto getNthArrivalDelayAirport(int rank) {
        return flightDao.getRandkArrivalAverageDelay(rank);
    }

    public DayOfWeekDto getNthBusyDayOfWeek(int rank) {
        return flightDao.getRankBusyDayOfWeek(rank);
    }


}
