package flight.dto;

/**
 * Created by yunhangchen on 7/7/17.
 */
public class RouteDataObject {

    String origAirportId;

    String destAirportId;
    Integer flightCount;

    public String getOrigAirportId() {
        return origAirportId;
    }

    public void setOrigAirportId(String origAirportId) {
        this.origAirportId = origAirportId;
    }

    public String getDestAirportId() {
        return destAirportId;
    }

    public void setDestAirportId(String destAirportId) {
        this.destAirportId = destAirportId;
    }

    public Integer getFlightCount() {
        return flightCount;
    }

    public void setFlightCount(Integer flightCount) {
        this.flightCount = flightCount;
    }


}
