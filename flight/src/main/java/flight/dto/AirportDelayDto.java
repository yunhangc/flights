package flight.dto;

/**
 * Created by yunhangchen on 7/8/17.
 */
public class AirportDelayDto {

    String airport;
    int averageDelayMinutes;

    public String getAirport() {
        return airport;
    }

    public void setAirport(String airport) {
        this.airport = airport;
    }

    public int getAverageDelayMinutes() {
        return averageDelayMinutes;
    }

    public void setAverageDelayMinutes(int averageDelayMinutes) {
        this.averageDelayMinutes = averageDelayMinutes;
    }

}
