package flight.dto;

/**
 * Created by yunhangchen on 7/8/17.
 */
public class DayOfWeekDto {

    String dayOfWeek;
    int numOfFlights;

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public int getNumOfFlights() {
        return numOfFlights;
    }

    public void setNumOfFlights(int numOfFlights) {
        this.numOfFlights = numOfFlights;
    }

    public static final String[] daysOfWeek = new String[]{"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

    public DayOfWeekDto(int dayOfWeek, int numOfFlights) {
        this.numOfFlights = numOfFlights;
        this.dayOfWeek = daysOfWeek[dayOfWeek - 1];
    }

}
