package flight.dto;

import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Map;

/**
 * Created by yunhangchen on 7/7/17.
 */
public class RouteDto {

    String departAirport;
    String departCity;
    String departState;

    String destAirport;
    String destCity;
    String destState;

    Integer totalFlights;

    public static final String SEARCH_ORIG_AIRPORT = "select * from flight_info where ORIGIN_AIRPORT_ID = ? limit 1;";

    public static final String SEARCH_DEST_AIRPORT = "select * from flight_info where DEST_AIRPORT_ID = ? limit 1;";

    public static RouteDto buildRouteDto(RouteDataObject routeDataObject, JdbcTemplate jdbcTemplate) {

        Map<String, Object> orig = jdbcTemplate.queryForList(SEARCH_ORIG_AIRPORT
                , new Object[] {routeDataObject.getOrigAirportId()}).get(0);

        Map<String, Object> dest = jdbcTemplate.queryForList(SEARCH_DEST_AIRPORT
                , new Object[] {routeDataObject.getDestAirportId()}).get(0);

        RouteDto dto = new RouteDto();
        dto.setDepartAirport((String) orig.get("ORIGIN"));
        dto.setDepartCity((String) orig.get("ORIGIN_CITY_NAME"));
        dto.setDepartState((String) orig.get("ORIGIN_STATE_ABR"));

        dto.setDestAirport((String) dest.get("DEST"));
        dto.setDestCity((String) dest.get("DEST_CITY_NAME"));
        dto.setDestState((String) dest.get("DEST_STATE_ABR"));
        dto.setTotalFlights(routeDataObject.getFlightCount());

        return dto;
    }

    public String getDepartAirport() {
        return departAirport;
    }

    public void setDepartAirport(String departAirport) {
        this.departAirport = departAirport;
    }

    public String getDepartCity() {
        return departCity;
    }

    public void setDepartCity(String departCity) {
        this.departCity = departCity;
    }

    public String getDepartState() {
        return departState;
    }

    public Integer getTotalFlights() {
        return totalFlights;
    }

    public void setTotalFlights(Integer totalFlights) {
        this.totalFlights = totalFlights;
    }

    public void setDepartState(String departState) {
        this.departState = departState;
    }

    public String getDestAirport() {
        return destAirport;
    }

    public void setDestAirport(String destAirport) {
        this.destAirport = destAirport;
    }

    public String getDestCity() {
        return destCity;
    }

    public void setDestCity(String destCity) {
        this.destCity = destCity;
    }

    public String getDestState() {
        return destState;
    }

    public void setDestState(String destState) {
        this.destState = destState;
    }

}
