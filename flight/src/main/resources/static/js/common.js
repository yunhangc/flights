var BASE_URL = "/";
var RANK_HOLDER = "{RANK_HOLDER}";

var FREQUENT_ROUTE = "flights/frequentRoute/rank/" + RANK_HOLDER;

var CARRIER_RANK = "flights/carrier/rank/" + RANK_HOLDER;

var AIRPORT_DEPARTURE_DELAY_RANK = "flights/delay/departure/" + RANK_HOLDER;

var AIRPORT_ARRIVAL_DELAY_RANK = "flights/delay/arrival/" + RANK_HOLDER;

var BUSY_DAY_OF_WEEK_RANK = "flights/dayOfWeek/" + RANK_HOLDER;
