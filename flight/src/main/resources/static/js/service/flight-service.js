var flightService = angular.module("flight-service", []);

flightService.provider('FlightDataService', function () {
  this.$get = ["$http", function ($http) {
        var functions = {

            executeGetRequest(url, successCallbackFunc, errorCallBackFunc) {
                $http({
                  method: 'GET',
                  url: url
                })
                   .success(function (data) {
                     successCallbackFunc(data);
                   })
                   .error(function (data) {
                     errorCallBackFunc(data);
                   });
            },

            getFrequentRoute: function(rank, successCallbackFunc, errorCallBackFunc) {
                functions.executeGetRequest(FREQUENT_ROUTE.replace(RANK_HOLDER, rank), successCallbackFunc, errorCallBackFunc);
            },

            getBusyCarrier: function(rank, successCallbackFunc, errorCallBackFunc) {
                functions.executeGetRequest(CARRIER_RANK.replace(RANK_HOLDER, rank), successCallbackFunc, errorCallBackFunc);
            },

            getDepartureDelayAirport: function(rank, successCallbackFunc, errorCallBackFunc) {
                functions.executeGetRequest(AIRPORT_DEPARTURE_DELAY_RANK.replace(RANK_HOLDER, rank), successCallbackFunc, errorCallBackFunc);
            },

            getArrivalDelayAirport: function(rank, successCallbackFunc, errorCallBackFunc) {
                functions.executeGetRequest(AIRPORT_ARRIVAL_DELAY_RANK.replace(RANK_HOLDER, rank), successCallbackFunc, errorCallBackFunc);
            },

            getBusyDayOfWeek: function(rank, successCallbackFunc, errorCallBackFunc) {
                functions.executeGetRequest(BUSY_DAY_OF_WEEK_RANK.replace(RANK_HOLDER, rank), successCallbackFunc, errorCallBackFunc);
            }

        }
        return functions;
  }];
});
