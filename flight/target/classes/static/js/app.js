angular.module('app', ['flight-service'])
.controller("AppController", ["$scope", "FlightDataService", function($scope, FlightDataService) {

    var frequentRouteSuccessCallBack = function(data) {
        $scope.route = data;
        $scope.showFrequentRouteResult = true;
        $scope.showFrequentRouteError = false;
    }

    var busyCarrierSuccessCallBack = function(data) {
        $scope.carrier = data;
        $scope.showBusyCarrierResult = true;
    }

    var departureDelayAirportSuccessCallBack = function(data) {
        $scope.departureDelayAirport = data;
        $scope.departureDelayResult = true;
    }

    var arrivalDelayAirportSuccessCallBack = function(data) {
        $scope.arrivalDelayAirport = data;
        $scope.arrivalDelayResult = true;
    }

    var getBusyDayOfWeekSuccessCallBack = function(data) {
        $scope.dayOfWeek = data;
        $scope.busyDayOfWeekResult = true;
    }

    var errorCallBack = function(data) {
        $scope.error = data.message;
    }

    $scope.getMostFrequentRoute = function (ranking) {
        $scope.showFrequentRouteResult = false;
        FlightDataService.getFrequentRoute(ranking, frequentRouteSuccessCallBack, errorCallBack);
    }

    $scope.getBusyCarrier = function(ranking) {
        $scope.showBusyCarrierResult = false;
        FlightDataService.getBusyCarrier(ranking, busyCarrierSuccessCallBack, errorCallBack);
    }

    $scope.getDepartureDelayAirport = function(ranking) {
        $scope.departureDelayResult = false;
        FlightDataService.getDepartureDelayAirport(ranking, departureDelayAirportSuccessCallBack, errorCallBack);
    }

    $scope.getArrivalDelayAirport = function(ranking) {
        $scope.arrivalDelayResult = false;
        FlightDataService.getArrivalDelayAirport(ranking, arrivalDelayAirportSuccessCallBack, errorCallBack);
    }

    $scope.getBusyDayOfWeek = function(ranking) {
        $scope.busyDayOfWeekResult = false;
        FlightDataService.getBusyDayOfWeek(ranking, getBusyDayOfWeekSuccessCallBack, errorCallBack);
    }
}]);